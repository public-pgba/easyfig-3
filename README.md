# EasyFig 3 - version 3.2.1

<img src="Easyfig.png" alt="Easyfig" width="150"/>

Easyfig 3 is a tool for graphical vizualisation of annotated genetic regions, and homologous regions comparison. It is an independent recoding of Easyfig 2 initially developped by  Mitchell J. Sullivan [https://mjsull.github.io/Easyfig/]

Authors: LECLERCQ Sébastien - BRANGER Maxime

## Licence
Creative Commons Attribution-ShareAlike 4.0 International  
  
<img src="https://creativecommons.org/images/deed/cc_blue_x2.png" alt="CC" width="50"/>
<img src="https://creativecommons.org/images/deed/attribution_icon_blue_x2.png" alt="BY" width="50"/>
<img src="https://creativecommons.org/images/deed/sa_blue_x2.png" alt="SA" width="50"/>  

CC BY-SA 4.0


## Installation
### On Linux/MacOS
#### Conda
```
conda env create -f requirements.yml
./Easyfig
```
You can also add 'Easyfig' to your PATH.  

### On Windows
#### Conda
open an anaconda powersheel terminal, then run this (can take a while) :
```
conda env create -f requirements_windows.yml
conda activate easyfig
pip3 install cairosvg
conda deactivate  
```
To launch Easyfig 3, you need to open an anaconda powersheel terminal, then 
```
conda activate easyfig
python Easyfig.py
conda deactivate  
```
These steps can be done in an executable .bat file.   

Blast is not included in the conda environment for Windows.  
You need to install it on your own : [https://www.ncbi.nlm.nih.gov/books/NBK52637] (NCBI)  
Please pay attention to the Configuration section of NCBI tutorial to correctly configure your installation.  
blast executables must be in your env PATH. 


## Interface
### General

<img src="helpimages/general.png" alt="General panel" width="1000"/>

General settings are prposed on top of the application.  

You can manually set the figure output width (the height is automatically determined by the number of sequences to print).  
Left/right/top/bottom margins can also manually be determined independently. 

The output figure path and name can be set by clicking on the `Draw figure in` icon. 

The `CREATE FIGURE` button generates the figure in SVG or PNG format. If no figure path and name is set when pressing it, a prompt window will ask you to. 

See [Descriptions options](#desc_general) for more information 

### Saving/Loading

<img src="helpimages/menu.png" alt="General panel" width="1000"/>

By clicking on the `Application` menu, you will be able to save your current project (every sequences and fields) with `Save/Save as...`, or only the configuration (features, blast, legends and decorum) with `Save configuration`.
You will also be able to load previous saved projects with `Open` or configuration files with `Load configuration`. Loading a configuration file will keep already imported sequences and apply the saved feature/legend/decorum/blast configuration on them.


### Status bar

<img src="helpimages/Status-start.png" alt="Status bar" width="1000"/>

Information about actions is displayed in the status bar at the bottom of the application. Standard information are in black, while it turns in green when the figure was successfully created or in red when an error occurred.




### Sequences panel

<img src="helpimages/sequence.png" alt="Sequences panel" width="1000"/>

This panel provides all parameters for each imported sequence. The `General` line defines default values that will be used if no specific parameter is set for a sequence (i.e. when '-' is set in a field). 

Sequences in fasta or GenBank formats can be imported using the `+` icon. Only nucleotide/genomic sequences are accepted. Multi-fasta or -Genbank are accepted ; each internal sequence will be imported as an independent sequence. The sequence name is equal to the LOCUS field in the GenBank file and cannot be modified in Easyfig 3. Sequence identification displayed in the figure can be changed using the `label type` field (species, description, size,...). 

GenBank sequence information is provided by clicking on the `infos` icon. If any information (except the LOCUS field) is modified in GenBank files while EasyFig is running, the file must be loaded again using the `reload` button from the sequence information pop-up.

Sequences can be deleted using the `cross` icon on the left of the sequence name. Beware, there is currently no warning for sequence deletion !

You can enable/disable a sequence drawing with the `active` field. Sequence ordering can be changed by dragging up and down the sequence name.

Vertical space between sequences can be changed using the `space below` field.

See [Descriptions options](#desc_seq) for information on each field  


### Features panel

<img src="helpimages/Features.png" alt="Features panel" width="1000"/>

This panel will help you to display the features included in the GenBank annotation. You can add as many as you want using the `+` icon. Like sequences, the `General` line defines common parameters if no specific value is set up for a feature's field.
You can choose to display or not a feature with the `selected` field. 

Feature definitions can be deleted or copied using the `cross` and `copy` icons on the left of the feature name. 

The most important parameter for features is the `filter` field. It will help you define on which features to apply the selected parameters. For instance, defining a CDS feature with a `fill color` set as green and a `filter` set as "kinase" will print in green all CDS having the term "kinase" in any of the fields of the GenBank file. The `in field` field allows you to reduce the scope of the seach to a specific field of the feature (gene, product, etc.). 

Values in the `filter` field are interpreted using regex syntax, in a case-insensitive way. This means that the OR operator '|' or metacharacters such as \d+ will work. More information on regex syntax here: https://www3.ntu.edu.sg/home/ehchua/programming/howto/Regexe.html   

Features definition are interpeted from top to bottom, and only the last valid definition will be selected for each feature in the figure. For instance, a feature definition with `filter` set to "kinase" will never be applied if another feature definition with `filter` set to "kin" is present lower. Features ordering can be changed by dragging up and down the feature name.


See [Descriptions options](#desc_feat) for more information   


### Blasts panel

<img src="helpimages/Blasts.png" alt="Blasts panel" width="1000"/>

Homologies between sequences can be inferred using BlastN or TBlastX. When all sequences are loaded, the `Run BlastN` button produces an All-vs-All blast output which needs to be saved on the disk. Blast needs to be run again only if new sequences are imported. Various parameters allow you to filter which blast hits will be displayed, from the total hits stored in the Blast file.  

Homologies can be displayed between adjacent sequences, between all sequences, or all against a single sequence using the `Matches selection` area. Used-defined homology comparisons can also be set up using the `Custom` option.

By default, no homology is displayed even if a Blast was performed.

The TBlastX area is hidden by default and can be displayed by clicking on the `TBLASTX` square button.

See [Descriptions options](#desc_blst) for more information   

### Legends panel

<img src="helpimages/Legends.png" alt="Legends panel" width="1000"/>

This panel allows you to manage legends displayed on the figure. By default, only the scale is printed. You can easily enable or disable each type of legend and choose where to print them out. The `Scaling` parameter is a factor of the figure size. Blast legends use the vertical figure size while features and scale legends use the horizontal figure size. The scale is automatically reduced to the closest informative size (1Kbp, 2Kbp, 10Kbp, 20Kbp,...). 

The features legend uses feature names as identifiers. Feature names can be used-defined in the `Feature` panel by simply clicking on it. Spaces are allowed, but try to avoid other special characters as much as possible.

Beware that legends are displayed in the margins of the figure. If a legend overlaps the sequences, you can enlarge the corresponding margin in the figure's General options.

See [Descriptions options](#desc_leg) for more information   


### Decorum panel

<img src="helpimages/decorum.png" alt="Decorum panel" width="1000"/>

This panel allows you to manage some additional sequence information, like GC%, GC skew or sequence-specific scales.  
This works like Sequences and Features panels. 

For GC% and GC skew information, the value is calculated every X nucleotides, given by the field `step`, on a fragment of size Y given by the `window` field.  
For the Scale information, `step` and `window` fields represent minor and major tick bars, respectively.  

Decorum definitions can be removed or copied using the `cross` and `copy` icons on the left of the decorum name. Decorum names can be user-defined by clicking on them. Decorum ordering can be changed by dragging up and down the decorum name.

By default, decorums are displayed for all sequences. They can be displayed on only on some sequences by using the `show on sequences` icon field which will opens a pop-up menu for sequence selection. 

See [Descriptions options](#desc_deco) for more information   

## Example of generated figure
This example shows the figure generated with the above options

<img src="helpimages/Figure1.png" alt="Example output" width="1000"/>


## Description of all options
You will find below the description for every options fields available.
### <a id="desc_general"></a>General
| Option | Description | Values |
|-------------|-------------|-------------|
| Image width | Define the exact size of the image width. This is a fix value in point or pixel. | Integer |
| Figure path | Define the output file path of the figure. | - |
| Margins | Define the exact margins size of the figure. | Integer |
| Background color | Define the background color of the image. | - |
| Enhance graph | Add some shadow effects to features | Checkbox |

### <a id="desc_seq"></a>Sequences
| Option | Description | Values |
|-------------|-------------|-------------|
| Sequence name | Name of the sequence | Automatically given according to the gbk/fasta LOCUS value |
| infos | Provide information about the sequence | - |
| active | Uncheck to not consider this sequence on drawing | Checkbox |
| height | Define th height of the sequence on the image | Integer |
| line width | Define the sequence line width | Integer |
| space below | Define the space below the current sequence | Integer |
| line color | Define the color of the sequence line | - |
| position | Determine the position of the sequence (inline) | left/right/center/best blast/custom |
| min | Position to consider to start on the sequence (default=1) | Integer |
| max | Position to consider to end on the sequence (default=max)| Integer or 'max' |
| reverse | Check to reverse the sequence | Checkbox |
| print label | Uncheck to hide sequence label | Checkbox |
| label type | Select the information to print as label | locus/species/description/locus+size/locus+species/locus+description |
| label position | Select the sequence label position  | left/right/top/bottom/top-left/top-right/bottom-left/bottom-right |
| offset to seq | Offset value between sequence and label | Integer |
| label color | Select sequence label color | - |
| label size | Define sequence label size | Integer |
| print feat. label | Check to display features labels | Checkbox |
| feat. label type | Select information to print as features label | gene/product/note/locus_tag/mobile_element_type |
| feat. label position | Define features label position | top/middle/bottom |
| feat color | Define features label color | - |
| feat label size | Define features label size | Integer |
| feat label rot | Define features label rotation (default=0) | Integer |

### <a id="desc_feat"></a>Features 
| Option | Description | Values |
|-------------|-------------|-------------|
| selected | Allow you to select/unselect a feature for the drawing | Checkbox |
| show in legend | Determine if this feature will be show in legend  | Checkbox |
| type | Select the type of feature  | CDS/gene/mobile_element |
| strand | Select the strand of the feature | none/lead up/lag up |
| shape | Define the shape of the feature on the draw | arrow/rectangle/frame/signal/range/rangeL |
| hatching | Define the hatching of the feature on the draw | none/hbar/hbar2/circle/crossed/crossed2/rain/diagonal/Rdiagonal/waved/dotted/crosses |
| height ratio | Define the ratio of the feature comparing to sequence height | Float |
| fill color | Color of the feature  | - |
| fill | Fill or not | Checkbox |
| line width | Line width | Integer |
| line color | Line color | - |
| filter | Using a regex to select some specific features by name/product or anything | Regex |
| in field | Fields to apply the regex filter | any/gene/product/note/mobile_element_type |
| print label | Print feature label or not | Checkbox |
| label type | Which label type to print  | gene/product/note/locus_tag/mobile_element_type |
| label position | Position of the label | Opposite/top/middle/bottom |
| label color | Color of the label | - |
| label size | Size of the label | Integer |
| label rot | Rotation of the label | Integer |

### <a id="desc_blts"></a>Blasts
| Option | Description | Values |
|-------------|-------------|-------------|
| Run BlastN |  | - |
| Load file |  | - |
| minimum length |  | Integer |
| minimum similarity |  | Integer |
| minimum e-value |  | Float |
| distance to sequence |  | Integer |
| color (min/max) |  | - |
| reversed color |  | Checkbox |
| opacity |  | Float |
| outline matches |  | Checkbox |
| show labels |  | Checkbox |
| min. match size |  | Integer |
| label color |  | - |
| label size |  | Integer |
| decimals |  | Integer |
| Matches selection |  | None/adjacent/All vs All/All vs one/Custom |

### <a id="desc_leg"></a>Legends
| Option | Description | Values |
|-------------|-------------|-------------|
| General display | Display/Hide this legend | Checkbox |
| Horizontal position | Select the horizontal position of the legend | left/middle/right |
| Vertical position | Select the vertical position of the legend | top/middle/bottom  |
| Scaling | Define a scale ratio for this legend. (default=0.3) | Float |
| Font Size | Define the font size of the legend | Integer |
| Font color | Define the font color of the legend | - |
| Cols number | Determine the exact number of columns to use to display features legend (default=1) | Integer |

### <a id="desc_deco"></a>Decorum
| Option | Description | Values |
|-------------|-------------|-------------|
| selected | Allow you to select/unselect the decorum for the drawing | Checkbox |
| show on sequences | This button opens a popup which allows you to select/unselect sequences to print this decorum | Switches on popup |
| type | Select which type of decorum to draw | GCskew/GC%/scale |
| step | Step size in bases for calculations | Integer |
| window | Window size in bases for calculations | Integer |
| position | Position of the decorum in relation to the sequence | On sequence/above/below |
| height ratio | Define the ratio of the decorum comparing to sequence height | Float |
| line width | Line width | Integer |
| line color | Line color | - |
| reverse | Reverse min/max position | Checkbox |
| print label | Print or not the decorum label  | Checkbox |
| label color | Color of the label | - |
| label size | Size of the label | Integer |
| label position | Position of the label in relation to decorum | left/right |

## Logo 
Our logo is using a CC BY 3.0 licenced image : [Dna Icon](https://iconscout.com/icons/dna)[Designed by vecteezy](https://iconscout.com/contributors/vecteezy) 
